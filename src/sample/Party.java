package sample;

import java.util.ArrayList;
import java.util.Date;

public class Party {
    private ArrayList<Person> allPersons;
    private String adress;
    private String date;
    private String inventationText;
    private Host host;

    public Party(ArrayList<Person> allPersons, String adress, String date, String inventationText, Host host) {
        this.allPersons = allPersons;
        this.adress = adress;
        this.date = date;
        this.inventationText = inventationText;
        this.host = host;
    }



    public ArrayList<Person> getAllPersons() {
        return allPersons;
    }

    public String getAdress() {
        return adress;
    }

    public String getDate() {
        return date;
    }

    public String getInventationText() {
        return inventationText;
    }

    public Host getHost() {
        return host;
    }

    public void sendInventation() {

    }
}
