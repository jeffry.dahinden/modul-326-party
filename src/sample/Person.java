package sample;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Person {
    private String firstName;
    private String lastName;
    private ArrayList<String> calendar = new ArrayList<String>();

    public Person(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
       // calendar.add("13/12/2020");

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public List<String> getCalendar() {
        return calendar;
    }

    public boolean acceptInventation(String date, String inventationText) {
       if (checkIfBusy(date)){
           System.out.println(getFirstName() + " Sie sind and dem Datum: " + date + " beschäftigt!");
           return false;
       }
        System.out.println(this.firstName + " Sie sind Frei!");
        System.out.println(inventationText + " Kommst du auch?");
        Scanner scan = new Scanner(System.in);
        String wantToGo = scan.next();
        if (wantToGo.equals("ja")) {calendar.add(date);}
        return wantToGo.equals("ja");
    }

    public boolean checkIfBusy(String date){
        for (String oneDate: calendar) {
            System.out.println(oneDate + " . " + date);
            if (oneDate.equals(date)){
                System.out.println("nichtFrei");
                return true;
            }
        }
        return false;
    }
}
