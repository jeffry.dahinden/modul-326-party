package sample;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Host {
    private Person person;



    public Host(Person person) {
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }

    public Party planParty(ArrayList<Person> invitedPersons, String date, String adress, String inventationText){
        ArrayList<Person> allPersons = new ArrayList<Person>();
        for (Person person: invitedPersons) {
            if(person.acceptInventation(date, inventationText)) {
                allPersons.add(person);
            }
        }
          /*  System.out.println("es Kommen:" );
            for (Person person1: allPersons){
                String fullName = person1.getFirstName() + " : " + person1.getLastName();
                System.out.println(fullName);
            }*/
            return new Party(allPersons, adress, date, inventationText, this);

    }


}
