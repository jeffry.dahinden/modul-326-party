package sample;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println("Willkommen zu dem PartyPlanner");
        ArrayList<Person> allPersons = getAllPersons();
        Scanner scan = new Scanner(System.in);
        System.out.println("wie ist ihr Vorname?");
        String firstname = scan.next();
        System.out.println("wie ist ihr Nachname?");
        String lastName = scan.next();
        Person protagonistPerson = new Person(firstname, lastName);
        Host protagonist = new Host(protagonistPerson);
        String wantToPlan;
        do {
            System.out.println("Wollen sie eine Party planen?");
            wantToPlan = scan.next();
            if (wantToPlan.equals("ja")) {

                Date date = new Date();
                String dateStr;
                boolean hasparsed = false;
                // String sDate1="31/12/1998";
                do {
                    System.out.println("wann ist das datum?");
                    dateStr = scan.next();
                    try  {
                        date = new SimpleDateFormat("dd/MM/yyyy").parse(dateStr);
                        hasparsed = true;

                    } catch (Exception e){
                        System.out.println("Kein Datum");
                    }
                } while (!hasparsed);


                System.out.println("Wo sollte die Party sein?");
                String address = scan.next();
                System.out.println("Bitte schreiben sie den EinladungsText: ");
                String inventationText = scan.next();
                Party party = protagonist.planParty(allPersons, dateStr, address, inventationText);
                System.out.println("Ihre Party wurde erstellt: ");
                System.out.println("Die Party ist in " + party.getAdress());
                System.out.println("Es Kommen: ");
                System.out.println("Host: " + party.getHost().getPerson().getFirstName() + " : " + party.getHost().getPerson().getLastName());
                for (Person person: party.getAllPersons()){
                    System.out.println(person.getFirstName() + " : " + person.getLastName());
                }
                System.out.println("Die Party ist am " + party.getDate());
            }
        } while (wantToPlan.equals("ja"));
    }

    public static ArrayList<Person> getAllPersons() {
        ArrayList<Person> persons = new ArrayList<>();
        Person person1 = new Person("Tom", "Müller");
        Person person2 = new Person("Peter", "Müller");
        Person person3 = new Person("Manuel", "Müller");
        Person person4 = new Person("Hans", "Müller");
        Person person5 = new Person("Bruno", "Kameraman");

        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        persons.add(person4);
        persons.add(person5);
        return persons;
    }
}
